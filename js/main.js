// Global Variable
var hexCase = 'upper';
var removeDupes = false;
var values = [];

function extendHex(h) {
    if (h.length < 7) {
        return '#' + h[1] + h[1] + h[2] + h[2] + h[3] + h[3];
    }
    return h;
}

function hexToRgb(hex) {
    function cutHex(h) {
        return (h.charAt(0) == "#") ? h.substring(1, 7) : h;
    }
    var r = parseInt((cutHex(hex)).substring(0, 2), 16);
    var g = parseInt((cutHex(hex)).substring(2, 4), 16);
    var b = parseInt((cutHex(hex)).substring(4, 6), 16);
    return r + ',' + g + ',' + b;
}

function rgbToHex(rgb) {
    function convert(n) {
        if (n.toString(16).length < 2) {
            return "0" + n.toString(16);
        } else {
            return n.toString(16);
        }
    }
    var r = parseInt((/\(([0-9 ]*),/g).exec(rgb)[1]);
    var g = parseInt((/,([0-9 ]*),/g).exec(rgb)[1]);
    var b = parseInt((/,([0-9 ]*)\)/g).exec(rgb)[1]);
    var hex = ('#' + convert(r) + convert(g) + convert(b));
    return extendHex(hex.toUpperCase());
}

function parseHex(input) {
    // hex input
    var cleanHex = {
        hex: [],
        rgb: [],
        rgba: []
    }
    for (var i = 0; i < input.length; i++) {
        var hex = extendHex(input[i].toUpperCase());
        var rgb = 'rgb(' + hexToRgb(hex) + ')';
        var rgba = 'rgba(' + hexToRgb(hex) + ',0.7)';
        cleanHex['hex'].push(hex);
        cleanHex['rgb'].push(rgb);
        cleanHex['rgba'].push(rgba);
    }
    return cleanHex;
}

function parseRgb(input) {
    // rgb(a) input
    var cleanRgb = {
        hex: [],
        rgb: [],
        rgba: []
    }
    for (var i = 0; i < input.length; i++) {
        var redBlueGreen = input[i].match(/[0-9 ]*,[0-9 ]*,[0-9 ]*/g);
        var rgb = 'rgb(' + redBlueGreen + ')';
        var rgba = 'rgba(' + redBlueGreen + ',0.7)';
        var hex = rgbToHex(rgb);
        cleanRgb['hex'].push(hex);
        cleanRgb['rgb'].push(rgb);
        cleanRgb['rgba'].push(rgba);
    }
    return cleanRgb
}

function parseInput(text) {
    function removeDuplicates(arr) {
        let unique_array = Array.from(new Set(arr));
        return unique_array;
    }

    // Get inputs
    var rawInputHex = text.match(/#([A-F0-9]{6}|[A-F0-9]{3})/g);
    var rawInputRgb = text.match(/RGB(|A)\(([0-9 ]*,[0-9 ]*,[0-9 ]*)(,|\))/g);


    // Determine what to return
    var results = {}
    if (rawInputHex != null & rawInputRgb != null) {
        var cleanHex = parseHex(rawInputHex);
        var cleanRgb = parseRgb(rawInputRgb);
        results = {
            hex: cleanHex['hex'].concat(cleanRgb['hex']),
            rgb: cleanHex['rgb'].concat(cleanRgb['rgb']),
            rgba: cleanHex['rgba'].concat(cleanRgb['rgba'])
        };
    } else if (rawInputHex != null & rawInputRgb == null) {
        results = parseHex(rawInputHex);
    } else if (rawInputHex == null & rawInputRgb != null) {
        results = parseRgb(rawInputRgb);
    } else {
        return;
    }

    // Remove duplicates if required
    if (removeDupes) {
        return {
            hex: removeDuplicates(results['hex']),
            rgb: removeDuplicates(results['rgb']),
            rgba: removeDuplicates(results['rgba'])
        };
    }
    return results;
}

function triggerAlert(message, alertType, timeout) {
    $('#alertModal').html(message);
    $('#alertModal').attr('data-alert-type', alertType);
    $('#alertModal').removeClass('hidden');
    setTimeout(function() {
        $('#alertModal').addClass('hidden');
    }, timeout);
}

function populateTable() {
    // Delete all existing rows of table
    $('#table-colour-viz tr').each(function() {
        $(this).remove();
    });
    $('#hexOutput').val('');
    $('#rgbOutput').val('');

    // Parse input text
    var input = $('#input').val().toUpperCase();
    values = parseInput(input, removeDupes);

    // Populate tables to visualise colours
    if (values != null) {
        hexValues = [];
        for (var i = 0; i < values['hex'].length; i++) {
            if (hexCase == 'lower') {
                var hexValue = values['hex'][i].toLowerCase()
            } else {
                var hexValue = values['hex'][i].toUpperCase()
            }
            var rgbValue = values['rgb'][i].toLowerCase()
            var rgbaValue = values['rgba'][i].toLowerCase()

            // Add rows each containing three cells:
            // 1. Translucent background displaying hex string
            // 2. Opaque background with copy icon for hex string
            // 3. Opaque background
            // 4. Opaque background with copy icon for rgb string
            // 5. Translucent background displaying rgb string
            $('#table-colour-viz').append(
                "<tr>" +
                "<td id='hex" + i + "' class='copiable td-hex rounded-left' data-clipboard-action='copy' data-clipboard-target='#hex" + i +
                "' style='background-color: " + rgbaValue + ";' align='left'>" + hexValue + "</td>" +
                "<td style='background-color: " + hexValue + ";'></td>" +
                "<td id='rgb" + i + "' class='copiable td-rgb rounded-right' data-clipboard-action='copy' data-clipboard-target='#rgb" + i +
                "' style='background-color: " + rgbaValue + ";' align='right'>" + rgbValue + "</td>" +
                "</tr>");
            hexValues.push(hexValue);
        }
        $(".dropdown-item").removeClass("disabled")
        if (removeDupes) {
            $('#removeDupes').addClass("disabled");
        }
    } else {
        triggerAlert('No hex or rgb values found', 'error', 1200);
    }
}

function copyToClipboard(content) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(content).select();
    document.execCommand("copy");
    $temp.remove();
}

$(document).ready(function() {
    // Copy single values
    var single_val_copy = new ClipboardJS('.copiable');

    single_val_copy.on('success', function(e) {
        console.info('Action:', e.action);
        console.info('Text:', e.text);
        console.info('Trigger:', e.trigger);

        e.clearSelection();

        $(e.trigger).popover('show');
        triggerAlert('Copied ' + e.text + ' to clipboard', 'info', 800);
    });

    single_val_copy.on('error', function(e) {
        console.error('Action:', e.action);
        console.error('Trigger:', e.trigger);
    });

    // Copy all hex values
    $("#batchCopyHex").on('click', function() {
        copyToClipboard(JSON.stringify(hexValues));
    });

    // Copy all rgb values
    $("#batchCopyRgb").on('click', function() {
        copyToClipboard(JSON.stringify(values['rgb']));
    });

    // View button
    $('#viewColours').on('click', function() {
        $('#removeDupes').css('visibility', 'visible');
        $('#removeDupes').prop('disabled', false);
        removeDupes = false;
        populateTable();
    });

    // Remove duplicates button
    $('#removeDupes').on('click', function() {
        if ($('#table-colour-viz tr').length > 0) {
            removeDupes = true;
            populateTable();
            $('#removeDupes').addClass("disabled");
        }
    });

    // Toggle hex case
    $('#hexCaseToggle').on('click', function() {
        if ($('#hexCaseToggle').html() == 'Convert hex to uppercase') {
            hexCase = 'upper'
            $('#hexCaseToggle').html('Convert hex to lowercase')
        } else {
            hexCase = 'lower'
            $('#hexCaseToggle').html('Convert hex to uppercase')
        }
        populateTable();
    })

    // Clear values buttton
    $('#clearValues').on('click', function() {
        $('#input').val('');
        location.reload();
    });

});