# Hex Viewer  
  
Hex Viewer displays the colour of multiple hex codes at once.  
It can take a text string and extract and display any hex codes within.  
  
![There's meant to be a .gif here](./README.gif)  
  
## Features  
  
### At first:  
* Text in the input field and `View` clicked &rarr; displays table of colours  
* If input field empty or does not contain hex or rgb values &rarr; error modal appears  
  
### When the table is visible:  
* rgb or hex value clicked &rarr; copies to clipboard and confirmation modal appears  
* `Clear values` clicked &rarr; input field clears and table removed  
* `Remove duplicates` clicked &rarr; duplicate colours removed from table  
* `Convert hex to lowercase` clicked &rarr; converts hex to lowercase in table  
* `Batch copy rgb values` clicked &rarr; copies list of all rgb values to clipboard  
* `Batch copy hex values` clicked &rarr; copies list of all hex values to clipboard  
